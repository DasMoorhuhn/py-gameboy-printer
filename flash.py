import esptool
import platform
import os
from urllib.request import urlretrieve as web_download

import tools
from config import Config


working_dir, filename = os.path.split(os.path.abspath(__file__))

config = Config().flasher
supported_platforms = [
  'ESP8266',
  'ESP32'
]


def download_firmware():
  with tools.silence_traceback():
    esp_type = tools.get_esp_info(port=config.port)[0]
  if esp_type not in supported_platforms: exit(1)

  url = f'{config.firmware_base_url}/{esp_type}_{config.firmware_type}-{config.firmware_version}.bin'
  file_name = f'{esp_type}_{config.firmware_type}-{config.firmware_version}.bin'
  print(f'Downloading firmware version {config.firmware_version} for {esp_type} to {file_name}')

  try:web_download(url=url, filename=file_name)
  except:print('Error during downloading')
  return file_name


binary = download_firmware()
erase_flash = [
  '--port', config.port,
  '--baud', config.baud,
  'erase_flash'
]

flash_binary = [
  '--port', config.port,
  '--baud', config.baud,
  'write_flash',
  '--flash_size', 'detect',
  '0', binary
]

# print('\n\nErase Flash...\n')
# esptool.main(erase_flash)

# print('\n\nFlash Binary...\n')
# esptool.main(flash_binary)

match platform.system():
  case 'Linux':
    os.system(f'sh flash_linux.sh {config.port} {config.baud} {binary}')

  case 'Windows':
    os.system(f'powershell.exe flash_windows.ps1 {config.port} {config.baud} {binary}')

# os.remove(binary)

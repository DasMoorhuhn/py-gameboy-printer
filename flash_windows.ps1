$port = $args[0]
$baud = $args[1]
$binary = $args[2]

esptool.py --port $port erase_flash
esptool.py --port $port --baud $baud write_flash --flash_size=detect -fm dout 0 $binary
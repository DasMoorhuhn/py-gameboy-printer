from time import sleep
H = "_|#"
L = "|"
N = "___"


counter = 0
while True:
  sleep(0.25)
  print(H, end='', flush=True)
  sleep(0.25)
  print(L, end='', flush=True)
  counter += 1
  if counter > 7:
    print(N, end='', flush=True)
    counter = 0

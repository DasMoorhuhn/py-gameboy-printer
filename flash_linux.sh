port=$1
baud=$2
binary=$3


esptool.py --port $port erase_flash
esptool.py --port $port --baud $baud write_flash --flash_size=detect -fm dout 0 $binary
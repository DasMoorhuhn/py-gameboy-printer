import esptool


def get_esp_info(port='/dev/ttyUSB0'):
  esp = esptool.detect_chip(port=port, baud=115200)
  mac = ":".join(map(hex, esp.read_mac())).replace('0x', '').upper()
  flash_kb = int(esp.FLASH_SIZES['256KB'])*256
  flash_mb = flash_kb // 1024 if (flash_kb / 1024) % 2 == 0 else flash_kb / 1024  # Checks if it'll be floating or decimal

  return esp.CHIP_NAME, mac, flash_kb, flash_mb


if __name__ == '__main__':
  from .search_esp import search_for_esp_devices
  esp_devs = search_for_esp_devices()
  if len(esp_devs) == 0: print("No ESP devices found")
  for esp_dev in esp_devs:
    info = get_esp_info(esp_dev[0])

    print('')
    print(f"Type:\n  {info[0]}\n")
    print(f"MAC:\n  {info[1]}\n")
    print(f"Flash:\n  {info[2]}KB\n  {info[3]}MB")


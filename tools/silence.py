import contextlib
import sys
import io


@contextlib.contextmanager
def silence_traceback():
    sys.stdout, old = io.StringIO(), sys.stdout
    try:yield
    finally:sys.stdout = old

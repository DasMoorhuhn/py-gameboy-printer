import esptool
import platform
import os
import io
import sys

from .silence import silence_traceback


def search_for_esp_devices(output=False):
  ports = []
  if platform.system() == 'Linux':
    devices = os.listdir(path='/dev')
    ports = [f"/dev/{device}" for device in devices if 'ttyUSB' in device]

  esp_devices = []
  for port in ports:
    try:
      with silence_traceback():
        esp = esptool.detect_chip(port=port, baud=115200, connect_attempts=1)
      if esp:
        device = [port, esp.CHIP_NAME]
        esp_devices.append(device)
        if output: print(f'Found {device[1]} on port {device[0]}')
    except:pass
  if output and len(esp_devices) == 0: print('No ESP devices found')
  return esp_devices


if __name__ == '__main__':
  search_for_esp_devices(output=True)

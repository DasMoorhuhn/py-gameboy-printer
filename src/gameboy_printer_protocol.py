from machine import Pin

# Commands
INIT_HEADER = 0x01
PRINT_HEADER = 0x02
DATA_HEADER = 0x04
INQUIRY_HEADER = 0x0F

# Magic Bytes
SYNC_HEADER_0 = 0x88
SYNC_HEADER_1 = 0x33

# Other
PRINTER_ACK = 0x81
COMPRESSION_DISABLED = 0x00
COMPRESSION_ENABLED = 0x01

# Printer State values
PRINTER_STATUS_CHECKSUM_ERROR = 0x00
PRINTER_STATUS_PRINTER_BUSY = 0x01
PRINTER_STATUS_IMAGE_DATA_FULL = 0x02
PRINTER_STATUS_UNPROCESSED_DATA = 0x03
PRINTER_STATUS_PACKET_ERROR = 0x04
PRINTER_STATUS_PAPER_JAM = 0x05
PRINTER_STATUS_OTHER_ERROR = 0x06
PRINTER_STATUS_BATTERY_LOW = 0x07

CONNECTED = False

# Pin definition
PIN_SO = 12   # Serial Output
PIN_SI = 13  # Serial Input (D7)
PIN_SD = 0  # Serial Data (D6)
PIN_SC = 14  # Serial Clock (D5)
PIN_LED = 2  # Build in LED (Wemos D1 Mini)


# Pin set
# LED = Pin(PIN_LED, Pin.OUT)
GB_PIN_SI = Pin(PIN_SI, Pin.OUT)
GB_PIN_SO = Pin(PIN_SO, Pin.IN)
GB_PIN_SC = Pin(PIN_SC, Pin.IN, Pin.PULL_UP)

# GB_PIN_SI.off()
GB_PIN_SI.off()


class GameboyPrinterProtocol:
  def __init__(self):
    self.CLOCK_STATE = False
    GB_PIN_SC.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=self.clock)

  def clock(self, pin):
    """If clock goes HIGH"""
    pin:Pin
    self.CLOCK_STATE = True if pin.value() == 1 else False

  def wait_for_connection(self):
    clock_temp_state = False
    count = 0
    packet = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x81, PRINTER_STATUS_CHECKSUM_ERROR]
    buffer = []
    while True:
      while self.CLOCK_STATE:
        print(f"SO: {GB_PIN_SO.value()}")
        print(f"Cycle: {count}")
        count += 1
        while not self.CLOCK_STATE:
          pass

      # print(self.CLOCK_STATE) if self.CLOCK_STATE else {}
      # if self.CLOCK_STATE != clock_temp_state:
      #   print(count)
      #   self.CLOCK_STATE = not self.CLOCK_STATE
      #   buffer.append(GB_PIN_SI.value())
      #   # GB_PIN_SI.value(packet[count])
      #   count += 1
      #   if count > 7:
      #     count = 0
      #     GB_PIN_SI.value(packet[count])
      #     print(buffer)
      #     buffer = []


      # if count > 11:
      #   count = 0
      # while CLOCK_STATE:
      #   print(GB_PIN_SI.value())
      #   GB_PIN_SO.value(packet[count])
      #   print(f"Packet {packet[count]}")
      #   count += 1
      #   while CLOCK_STATE: pass
      #   # print(count)
      #   # print(CLOCK_STATE)




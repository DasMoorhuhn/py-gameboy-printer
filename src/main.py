from src.gameboy_printer_protocol import GameboyPrinterProtocol


gameboy_printer = GameboyPrinterProtocol()


def start():
  gameboy_printer.wait_for_connection()

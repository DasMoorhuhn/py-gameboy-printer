import yaml


class Flasher:
  def __init__(self, data:dict):
    self.port = data['port']
    self.baud = data['baud']
    self.firmware_version = data['firmware_version']
    self.firmware_base_url = data['firmware_base_url']
    self.firmware_type = data['firmware_type']


class GameboyPrinter:
  def __init__(self, data:dict):
    pass


class Config:
  def __init__(self, config_file='config.yml'):
    with open(config_file, 'r') as file:
      config = yaml.safe_load(file)
    self.flasher = Flasher(config['flasher'])
    # self.gameboy_printer = GameboyPrinter(config['gameboy_printer'])


